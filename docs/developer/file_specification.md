Hash File Specification
=======================

Note: All data is stored in big-endian.

Header
------

The layout of the header is as follows:

|Specifier   |Size in bytes|Type     |
|------------|-------------|---------|
|Magic number|            8|uint8_t[]|
|Time base   |            8|   double|
|Frame count |            4|      int|

###### Magic number
The first eight bytes of the file contain the following hexadecimal values:

    85 48 41 53 48 92 95 0D

The signature specifies that the file is a video hash.

###### Time base
An eight byte double containing the time base for the hashed video.

###### Frame count
A four byte interger contain the number of frames that have been written to the file.

Data Body
---------

The rest of the file contains 16 byte chunks of data for each frame in the hashed video.
The layout of the data is as follows:

|Specifier|Size in bytes|Type    |
|---------|-------------|--------|
|Hash     |            8|uint64_t|
|Timestamp|            8|int64_t |

###### Hash
An eight byte unsigned integer containing the hash of the video frame.

###### Timestamp
An eight byte integer containing the timestamp of the video frame. The timestamp can be either DTS
or PTS, depending on the video file.
