User documentation
------------------

[Overview](user/overview.md) A basic overview of using the program.

Developer documentation
-----------------------

[How it Works](developer/how_it_works.md) An overview of how the program works internally.

[File specification](developer/file_specification.md) The specification for video-hash files.
