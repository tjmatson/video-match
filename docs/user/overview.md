Overview
========

Searching
---------

When searching for a video clip, the `--search` and `--full` arguments can be
used to explicitly specify video files, or they can be omitted and two video
files can be passed implicitly.

    $ video-match --search clip.mp4 --full full.mp4

Is the same as:

    $ video-match clip.mp4 full.mp4

In order to either limit or expand the amount of results returned, the
threshold can be changed. Increasing the threshold will give you less accurate
matches, while decreasing it will only give more accurate matches. The default
threshold is `0.2`, which should work for the majority of cases.

    $ video-match clip.mp4 full.mp4 -t 2
    Match at 0:4:23.56 with a deviation of 1.487
    Match at 0:8:18.39 with a deviation of 0.167

    $ video-match clip.mp4 full.mp4 -t 0.5
    Match at 0:8:18.39 with a deviation of 0.167

Hashing
-------

The hash of a video file can be pre-computed in order to speed up multiple uses
of a video. Instead of hashing the video each time it's used, it can be hashed
once and used multiple times. In order to hash a video, use the `--hash` and
`--out` options. The argument given to `--hash` will be the file path to the
video to hash, and the argument given to `--out` will be the file path to write
the hashed video to.

    $ video-match --hash full.mp4 -o full.hash

The hashed video can then be used in place of a normal video.

    $ video-match clip.mp4 full.hash

Options
-------

    -s --search      Video or hash file to search for.
    -f --full        Video or hash file to search through.
    -H --hash        Video file to hash.
    -o --out         File to write hashed video to.
    -t --threshold   Maximum threshold for a potential match to be displayed.
