Overview
--------

video-match will find all occurrences of a video within another video.

Installation
------------

    $ git clone git@gitlab.com:tjmatson/video-match.git
    $ cd video-match
    $ mkdir build
    $ cd build
    $ cmake ..
    $ make && sudo make install

Usage
-----

Find every occurrence of the video `clip.mp4` in the video `full.mp4`:

    $ video-match clip.mp4 full.mp4

Hash a video and write it to the file `full.hash`. Useful if you want to use a
video multiple times without having to hash it for each search:

    $ video-match --hash full.mp4 -o full.hash

A hash file can be used in place of a normal video file:

    $ video-match clip.mp4 full.hash

View the [docs](docs/README.md) for more information.
