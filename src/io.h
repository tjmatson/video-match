/*
 * Copyright (c) 2016 Timothy Matson
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef VIDEO_MATCH_IO_H
#define VIDEO_MATCH_IO_H

#include "hash.h"
#include "video.h"
#include "containers/hash_array.h"

#include <inttypes.h>

/* Header is 20 bytes long
 * Magic number - 8 bytes
 * Time base    - 8 bytes
 * Frame count  - 4 bytes
 */

static const int HEADER_SIZE = 20;
static const int DATA_SIZE = 16;

static const int TIME_BASE_LOC = 8;
static const int FRAME_COUNT_LOC = 16;

typedef struct FileContext
{
    FILE *file;
    const char *filename;
    double time_base;
    int frame_count;
    int data_pos;
} FileContext;

/* Convert an unsigned long integer from host byte order to network byte order,
 * storing the byte order in a buffer.
 */
void htonll_buffer(uint64_t host, uint8_t *buf);

/* Convert a network byte order buffer to a network byte order unsigned long integer. */
uint64_t btonll(uint8_t *buf);

/* Write a VideoState to file.
 * @return -2 if the file cant be opened, 0 otherwise
 */
int write_to_file(VideoState *video, const char *filename);

/* Initialize a file context.
 * @return -1 if the file isn't a hash, -2 if the file can't be opened, 0 otherwise
 */
int init_file_context(FileContext *context, const char *filename);

/* Compare the first few bytes of a file to the magic number.
 * @return -1 if the magic number doesn't match, 0 otherwise
 */
int compare_magic_number(FILE *f);

/* Check if a file is a hash file.
 * @return -1 if the file isn't a hash, -2 if the file can't be opened, 0 otherwise
 */
int is_file_hash(const char *filename);

/* Read the next set of data in the FileContext.
 * @param  data  Data struct to store hash and timestamp in
 * @param  eof   will be set to true when last set of data is read
 * @return -1 if EOF has been reached successfully, -4 if the amount of data
 * read is less than expected, 0 otherwise
 */
int read_hash_data(FileContext *fc, Hash *data, bool *eof);

/* Close the FileContext and reset its variables. */
void close_file_context(FileContext *context);

/* Read a hash file into a HashArray.
 * @param  hashes  input HashArray to store hashes in
 * @return -1 if the file can't be opened, 0 otherwise
 */
int file_to_hash_array(const char *filename, HashArray *hashes);

#endif // VIDEO_MATCH_IO_H
