/*
 * Copyright (c) 2016 Timothy Matson
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef VIDEO_MATCH_VIDEO_H
#define VIDEO_MATCH_VIDEO_H

#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <stdbool.h>

/* Video state container. */
struct VideoState
{
    AVFormatContext *fmtctx;
    AVCodecContext *context;
    AVCodec *codec;
    AVStream *video_st;
    int video_index; // The index of the video stream
};

typedef struct VideoState VideoState;

/* Decode a packet into a frame.
 * @param  avctx  codec context
 * @param  frame  destination frame
 * @param  pkt    input packet
 * @return 0 on success, otherwise neagtive error code
 */
int decode_frame(AVCodecContext *avctx, AVFrame *frame, AVPacket *pkt);

/* Decode the next frame of the video stream.
 * @param  video  input VideoState with video stream to decode
 * @param  frame  destination frame
 * @param  pkt    internal destination packet
 * @return 1 if no frame is decoded because the EOF is reached, 0 otherwise
 */
int next_video_frame(VideoState *video, AVPacket *pkt, AVFrame *frame);

/* Open a video file.
 *
 * Video must be closed with close_video().
 * @param  video     pointer to the VideoState
 * @param  filename  video file to open
 */
int open_video(VideoState *video, const char *filename);

/* Close an opened VideoState.
 *
 * All of its contents will be freed.
 */
void close_video(VideoState *video);

/* Get the framerate of a stream.
 * @param  stream  pointer to the stream
 * @return the stream's framerate
 */
double get_framerate(AVStream *stream);

/* Get the best estimate of the frame count of a video.
 *
 * If the stream's nb_frames isn't set, frame count will
 * be estimated from the video's duration and framerate.
 * @param  video  pointer to the video
 */
int best_estimate_frame_count(VideoState *video);

#endif // VIDEO_MATCH_VIDEO_H
