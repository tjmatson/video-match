cmake_minimum_required(VERSION 2.6)

find_package(FFMPEG REQUIRED)
include_directories(${FFMPEG_INCLUDE_DIRS})

set(COMPILE_FLAGS "-Wall -Wextra -g -gstabs+ -pedantic")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${COMPILE_FLAGS}")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/bin")

set(SOURCES hash.c video.c utils.c io.c parser.c)
set(HEADERS hash.h video.h utils.h io.h parser.h)
add_library(VideoMatch ${SOURCES} ${HEADERS})

add_subdirectory(containers)
target_link_libraries(VideoMatch containers)

add_executable(video-match video_match.c)
target_link_libraries(video-match VideoMatch ${FFMPEG_LIBRARIES} m)

install(TARGETS video-match DESTINATION "/usr/bin")
