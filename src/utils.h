/*
 * Copyright (c) 2016 Timothy Matson
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef VIDEO_MATCH_UTILS_H
#define VIDEO_MATCH_UTILS_H

#include <stdbool.h>

/* Converts seconds to the timestamp H:M:S.ms. */
char * seconds_to_timestamp(double seconds);

/* Checks if a string is a valid C double. Will fail if there is any unconsumed
 * data in the string.
 */
bool isdouble(char *str);

/* Returns true if either argument isn't NULL. */
bool either(const char *a, const char *b);

/* Returns true if both arguments aren't NULL. */
bool both(const char *a, const char *b);

/* Returns true if both arguments are NULL. */
bool neither(const char *a, const char *b);

/* Returns true if only the first argument isn't NULL. */
bool only_first(const char *a, const char *b);

#endif // VIDEO_MATCH_UTILS_H
