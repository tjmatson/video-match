/*
 * Copyright (c) 2016 Timothy Matson
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "video.h"

#include <stdbool.h>

int decode_frame(AVCodecContext *avctx, AVFrame *frame, AVPacket *pkt)
{
    av_frame_unref(frame);
    int ret = avcodec_send_packet(avctx, pkt);
    if (ret < 0)
    {
        return ret;
    }
    ret = avcodec_receive_frame(avctx, frame);
    if (ret < 0 && ret != AVERROR(EAGAIN) && ret != AVERROR_EOF)
    {
        fprintf(stderr, "Error while decoding frame\n");
    }

    return ret;
}

int next_video_frame(VideoState *video, AVPacket *pkt, AVFrame *frame)
{
    bool eof = false;
    while (eof == false)
    {
        if (av_read_frame(video->fmtctx, pkt) < 0)
        {
            eof = true;
            // Send a flush packet
            pkt->data = NULL;
            pkt->size = 0;
        }

        if (pkt->stream_index == video->video_index)
        {
            if (decode_frame(video->context, frame, pkt) >= 0)
            {
                return 0;
            }
        }
    }

    return 1;
}

int open_video(VideoState *video, const char *filename)
{
    video->fmtctx = avformat_alloc_context();
    if (avformat_open_input(&video->fmtctx, filename, NULL, NULL) != 0)
    {
        fprintf(stderr, "error: could not open video file \"%s\"\n", filename);
        return 1;
    }
    if (avformat_find_stream_info(video->fmtctx, NULL) < 0)
    {
        fprintf(stderr, "error: stream information not found for \"%s\"\n", filename);
        return 1;
    }

    // Get the index of the video stream
    video->video_st = NULL;
    video->video_index = -1;
    for (uint32_t stream = 0; stream < video->fmtctx->nb_streams; ++stream)
    {
        if (video->fmtctx->streams[stream]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            video->video_st = video->fmtctx->streams[stream];
            video->video_index = stream;
            break;
        }
    }
    if (video->video_st == NULL)
    {
        fprintf(stderr, "error: video stream not found for \"%s\"\n", filename);
        return -1;
    }

    video->context = avcodec_alloc_context3(NULL);
    if (video->context == NULL)
    {
        fprintf(stderr, "error: could not allocate video codec context for \"%s\"\n", filename);
        return -1;
    }
    if (avcodec_parameters_to_context(video->context, video->video_st->codecpar) < 0)
    {
        fprintf(stderr, "error: could not fill context with parameters for \"%s\"\n", filename);
        return -1;
    }

    video->codec = avcodec_find_decoder(video->context->codec_id);
    if (video->codec == NULL)
    {
        fprintf(stderr, "error: codec not found for \"%s\"\n", filename);
        return -1;
    }
    if (avcodec_open2(video->context, video->codec, NULL) < 0)
    {
        fprintf(stderr, "error: could not open codec for \"%s\"\n", filename);
        return -1;
    }

    return 0;
}

void close_video(VideoState *video)
{
    avcodec_close(video->context);
    av_free(video->context);
    video->video_st = NULL;
    video->codec = NULL;
    avformat_close_input(&video->fmtctx);
    video->video_index = -1;
}

double get_framerate(AVStream *stream)
{
    double fps = av_q2d(stream->avg_frame_rate);

    if (fps == 0)
    {
        fps = av_q2d(stream->r_frame_rate);
    }

    return fps;
}

int best_estimate_frame_count(VideoState *video)
{
    int num_frames = video->video_st->nb_frames;
    if (num_frames == 0)
    {
        // Estimate the number of frames
        int framerate = get_framerate(video->video_st);
        double duration = (double)video->fmtctx->duration / AV_TIME_BASE;
        num_frames = (int)ceil(framerate * duration);
    }

    return num_frames;
}
