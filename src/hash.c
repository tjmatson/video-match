/*
 * Copyright (c) 2016 Timothy Matson
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "video.h"
#include "hash.h"
#include "utils.h"
#include "io.h"
#include "containers/index_queue.h"
#include "containers/hash_array.h"

#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
#include <libavutil/pixfmt.h>

#include <inttypes.h>
#include <stdbool.h>
#include <limits.h>
#include <float.h>
#include <math.h>

const int HASH_WIDTH = 9;
const int HASH_HEIGHT = 8;
const int HASH_ALIGN = 32;

dHashContext dhash_context(enum AVPixelFormat pix_fmt, int width, int height)
{
    dHashContext context;
    context.srcH = height;

    context.data = av_frame_alloc();
    context.source = av_frame_alloc();

    int num_bytes = av_image_get_buffer_size(AV_PIX_FMT_GRAY8, HASH_WIDTH, HASH_HEIGHT, HASH_ALIGN);
    context.sws_buf = (uint8_t *)av_malloc(num_bytes * sizeof(uint8_t));

    av_image_fill_arrays(context.data->data, context.data->linesize, context.sws_buf,
                         AV_PIX_FMT_GRAY8, HASH_WIDTH, HASH_HEIGHT, HASH_ALIGN);
    context.linesize = context.data->linesize[0];

    context.swsctx = sws_getContext(
        width,
        height,
        pix_fmt,
        HASH_WIDTH,
        HASH_HEIGHT,
        AV_PIX_FMT_GRAY8,
        SWS_POINT,
        NULL,
        NULL,
        NULL
    );

    return context;
}

void free_dhash_context(dHashContext *context)
{
    av_frame_free(&context->data);
    av_frame_free(&context->source);
    av_free(context->sws_buf);
    sws_freeContext(context->swsctx);
    context->srcH = 0;
}

uint64_t dhash(dHashContext *context)
{
    // http://www.hackerfactor.com/blog/index.php?/archives/529-Kind-of-Like-That.html
    sws_scale(context->swsctx, (uint8_t const **)context->source->data,
              context->source->linesize, 0, context->srcH,
              context->data->data, context->data->linesize);

    uint8_t *frame = context->data->data[0];

    uint64_t hash = 0;
    int counter = 1;
    for (int i = 0; i < HASH_HEIGHT; ++i)
    {
        for (int j = 0; j < HASH_WIDTH - 1; ++j)
        {
            int pixel = i * context->linesize + j;
            if (frame[pixel] < frame[pixel + 1])
            {
                hash |= (uint64_t)1 << counter;
            }
            ++counter;
        }
    }

    return hash;
}

HashArray dhash_video(VideoState *video)
{
    int num_frames = best_estimate_frame_count(video);
    HashArray hashes = alloc_hash_array(num_frames);

    dHashContext dhash_ctx = dhash_context(video->context->pix_fmt,
                                           video->context->width,
                                           video->context->height);

    AVPacket pkt;
    av_init_packet(&pkt);

    while (next_video_frame(video, &pkt, dhash_ctx.source) == 0)
    {
        Hash hash = {dhash(&dhash_ctx), 0};
        hash_array_add(&hashes, hash);
        av_packet_unref(&pkt);
    }

    free_dhash_context(&dhash_ctx);

    return hashes;
}

int index_of_best_scoring_hash(uint64_t hash, HashArray *hashes, int next)
{
    static int next_index = 0;
    double best_score = DBL_MAX;
    int best_index;

    if (next != -1) { next_index = next; }

    for (uint32_t i = 0; i < hashes->count; ++i)
    {
        double score = bit_difference(hash, hashes->data[i].hash);
        // Favor hashes that are close to the previous hash in index; limit to 5
        score += (double)(5 * abs(i - next_index)) / hashes->count;
        if (score < best_score) // A lower score means a better match
        {
            best_score = score;
            best_index = i;
        }
        if (score == 0) { break; } // Best possible score; stop comparing hashes
    }

    next_index = best_index + 1;
    return best_index;
}

uint8_t bit_difference(uint64_t a, uint64_t b)
{
    uint64_t i = a ^ b;

    // Returns the bit count of a 64-bit integer
    // http://stackoverflow.com/a/2709523
    i = i - ((i >> 1) & 0x5555555555555555);
    i = (i & 0x3333333333333333) + ((i >> 2) & 0x3333333333333333);
    return (((i + (i >> 4)) & 0xF0F0F0F0F0F0F0F) * 0x101010101010101) >> 56;
}

double index_sequence_deviation(IndexQueue *indexes)
{
    double deviation = 0;

    for (uint32_t i = 0; i < indexes->count; ++i)
    {
        deviation += (double)abs(index_queue_get(indexes, i)->index - i) / indexes->count;
    }

    return deviation;
}

void print_match(int64_t timestamp, double time_base, double deviation)
{
    double seconds = timestamp * time_base;
    char *str = seconds_to_timestamp(seconds);

    printf("Match at %s with a deviation of %f\n", str, deviation);
    free(str);
}

void find_matches(VideoState *video, HashArray *hashes, double threshold,
        void(*resultFunc)(int64_t, double, double))
{
    IndexQueue indexes;
    alloc_index_queue(&indexes, hashes->count);
    dHashContext dhash_ctx = dhash_context(video->context->pix_fmt,
                                           video->context->width,
                                           video->context->height);
    AVPacket pkt;
    av_init_packet(&pkt);

    double lowest_deviation = DBL_MAX;
    int64_t timestamp;
    int eof = 0;
    while (eof == 0)
    {
        eof = next_video_frame(video, &pkt, dhash_ctx.source);
        if (eof == 0)
        {
            uint64_t hash = dhash(&dhash_ctx);
            int best_index = index_of_best_scoring_hash(hash, hashes, -1);
            FrameIndex index =
            {
                av_frame_get_best_effort_timestamp(dhash_ctx.source),
                best_index
            };
            index_queue_add(&indexes, index);
        }

        // Once enough frames have been processed, check to see if the
        // sequence of indexes scores below the threshold.
        if (indexes.count == hashes->count)
        {
            double deviation = index_sequence_deviation(&indexes);
            if (deviation < threshold && deviation < lowest_deviation)
            {
                timestamp = index_queue_get(&indexes, 0)->timestamp;
                lowest_deviation = deviation;
            }
            // To avoid reporting matches within one frame of each other, the lowest
            // deviation sequence won't be reported until the deviation rises above
            // the threshold, or, as a last resort, the End-of-File is reached.
            if (lowest_deviation != DBL_MAX &&
                    (deviation > threshold || eof == 1))
            {
                resultFunc(timestamp, av_q2d(video->video_st->time_base),
                            lowest_deviation);
                lowest_deviation = DBL_MAX;
                }
        }
        av_packet_unref(&pkt);
    }

    free_index_queue(&indexes);
    free_dhash_context(&dhash_ctx);
}

void find_matches_from_file(const char *filename, HashArray *hashes, double threshold,
        void(*resultFunc)(int64_t, double, double))
{
    FileContext fc;
    if (init_file_context(&fc, filename) != 0) { return; }

    IndexQueue indexes;
    alloc_index_queue(&indexes, hashes->count);

    int64_t timestamp;
    double lowest_deviation = DBL_MAX;
    Hash data;
    bool eof = false;
    while (eof == false)
    {
        read_hash_data(&fc, &data, &eof);
        int best_index = index_of_best_scoring_hash(data.hash, hashes, -1);
        FrameIndex index =
        {
            data.timestamp,
            best_index
        };
        index_queue_add(&indexes, index);

        // Once enough frames have been processed, check to see if the
        // sequence of indexes scores below the threshold.
        if (indexes.count == hashes->count)
        {
            double deviation = index_sequence_deviation(&indexes);
            if (deviation < threshold && deviation < lowest_deviation)
            {
                timestamp = index_queue_get(&indexes, 0)->timestamp;
                lowest_deviation = deviation;
            }
            // To avoid reporting matches within one frame of each other, the lowest
            // deviation sequence won't be reported until the deviation rises above
            // the threshold, or, as a last resort, the End-of-File is reached.
            if (lowest_deviation != DBL_MAX &&
                    (deviation > threshold || eof == true))
            {
                resultFunc(timestamp, fc.time_base, lowest_deviation);
                lowest_deviation = DBL_MAX;
            }
        }
    }

    free_index_queue(&indexes);
    close_file_context(&fc);
}
