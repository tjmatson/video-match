/*
 * Copyright (c) 2016 Timothy Matson
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef VIDEO_MATCH_ARRAY_H
#define VIDEO_MATCH_ARRAY_H

#include <stdint.h>
#include <stddef.h>

typedef struct Hash
{
    uint64_t hash;
    int64_t timestamp;
} Hash;

/* A dynamically growable array of hashes. */
typedef struct HashArray
{
    Hash *data; // Internal array of hashes
    size_t count; // Number of hashes currently in the array
    size_t size; // Size of the internal array
} HashArray;

/* Allocate memory for the HashArray and initialize its variables.
 * @param  size    initialize size of the HashArray
 * @return allcoated HashArray
 */
HashArray alloc_hash_array(size_t size);

/* Add a hash to a HashArray. If there isn't enough space to hold the hash,
 * the HashArray will reallocate some amount of memory to itself. */
void hash_array_add(HashArray *hashes, Hash hash);

/* Free the HashArray and reset its variables */
void free_hash_array(HashArray *hashes);

#endif // VIDEO_MATCH_ARRAY_H
