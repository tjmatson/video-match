/*
 * Copyright (c) 2016 Timothy Matson
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef VIDEO_MATCH_QUEUE_H
#define VIDEO_MATCH_QUEUE_H

#include <stdint.h>
#include <stddef.h>

/* Stores the index and timestamp of a frame */
typedef struct FrameIndex
{
    int64_t timestamp;
    size_t index;
} FrameIndex;

/* IndexQueue is a fixed-sized circular queue. Elements can only be inserted
 * into the queue, not popped off. If the queue is full, the front item is 
 * automatically pushed out of the queue.
 */
typedef struct IndexQueue
{
    FrameIndex *data; // Underlying array
    size_t front; // Index of the front of the queue
    size_t count; // Number of items in the queue
    size_t size; // Size of the queue
} IndexQueue;

/* Allocate memory for an IndeqQueue and initialize its variables 
 * @param  queue  IndexQueue to allocate and initialize
 * @param  size   initial size of the IndexQueue
 */
void alloc_index_queue(IndexQueue *queue, size_t size);

/* Insert an FrameIndex into the back of the queue. If the queue is full,
 * the first inserted item will be dropped from the queue.
 */
void index_queue_add(IndexQueue *queue, FrameIndex item);

/* Get the pointer to the FrameIndex at the given index in the queue.
 * @return NULL if index is out of bounds, otherwise the pointer to the FrameIndex
 */
FrameIndex * index_queue_get(IndexQueue *queue, size_t index);

/* Free the IndexQueue and reset the its variables */
void free_index_queue(IndexQueue *queue);

#endif // VIDEO_MATCH_QUEUE_H
