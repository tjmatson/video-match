/*
 * Copyright (c) 2016 Timothy Matson
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "hash_array.h"

#include <stdlib.h>
#include <stdio.h>

HashArray alloc_hash_array(size_t size)
{
    HashArray hashes;
    hashes.data = (Hash *)malloc(size * sizeof(Hash));
    hashes.count = 0;
    hashes.size = size;

    return hashes;
}

void hash_array_add(HashArray *hashes, Hash hash)
{
    if (hashes->size == hashes->count)
    {
        // Array growth calculation
        hashes->size = (hashes->size * 3) / 2 + 8;
        hashes->data = (Hash *)realloc(hashes->data, hashes->size * sizeof(Hash));
    }
    hashes->data[hashes->count] = hash;
    ++hashes->count;
}

void free_hash_array(HashArray *hashes)
{
    free(hashes->data);
    hashes->data = NULL;
    hashes->count = hashes->size = 0;
}
