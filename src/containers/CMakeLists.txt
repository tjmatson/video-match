set(CONTAINERS_SOURCE index_queue.c hash_array.c)
set(CONTAINERS_HEADER index_queue.h hash_array.h)

add_library(containers ${CONTAINERS_SOURCE} ${CONTAINERS_HEADER})
