/*
 * Copyright (c) 2016 Timothy Matson
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "index_queue.h"

#include <stdlib.h>

void alloc_index_queue(IndexQueue *queue, size_t size)
{
    queue->data = (FrameIndex *)malloc(sizeof(FrameIndex) * size);
    queue->size = size;
    queue->front = 0;
    queue->count = 0;
}

void index_queue_add(IndexQueue *queue, FrameIndex item)
{
    queue->data[queue->front] = item;
    queue->front = (queue->front + 1) % queue->size;
    if (queue->count < queue->size)
    {
        ++queue->count;
    }
}

FrameIndex * index_queue_get(IndexQueue *queue, size_t index)
{
    if (index >= queue->count)
    {
        return NULL;
    }
    return &queue->data[(queue->front + index) % queue->size];
}

void free_index_queue(IndexQueue *queue)
{
    free(queue->data);
    queue->size = 0;
    queue->front = 0;
    queue->count = 0;
}
