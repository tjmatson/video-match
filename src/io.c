/*
 * Copyright (c) 2016 Timothy Matson
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "io.h"

#include <arpa/inet.h>
#include <unistd.h>

static const uint64_t masks[] =
{
    0xFF00000000000000ULL,
    0x00FF000000000000ULL,
    0x0000FF0000000000ULL,
    0x000000FF00000000ULL,
    0x00000000FF000000ULL,
    0x0000000000FF0000ULL,
    0x000000000000FF00ULL,
    0x00000000000000FFULL
};

static const uint8_t magic_number[] = {0x85, 'H', 'A', 'S', 'H', 0x92, 0x95, '\n'};

void htonll_buffer(uint64_t host, uint8_t *buf)
{
    int shift = sizeof(buf) - 1;
    for (size_t i = 0; i < sizeof(buf); ++i)
    {
        buf[i] = (host & masks[i]) >> shift * sizeof(buf);
        --shift;
    }
}

uint64_t btonll(uint8_t *buf)
{
    uint64_t n = 0;

    int shift = sizeof(buf) - 1;
    for (size_t i = 0; i < sizeof(buf); ++i)
    {
        n |= (uint64_t)buf[i] << shift * sizeof(buf);
        --shift;
    }

    return n;
}

int write_to_file(VideoState *video, const char *filename)
{
    FILE *f = fopen(filename, "wb");
    if (f == NULL)
    {
        fprintf(stderr, "error: could not open \"%s\": %s\n", filename, strerror(errno));
        return -2;
    }

    // -- Header --
    // Write magic number
    fwrite(magic_number, sizeof(uint8_t), sizeof(magic_number), f);

    uint8_t buf[sizeof(uint64_t)];

    // Write time_base
    double time_base = av_q2d(video->video_st->time_base);
    uint64_t tmp; // Used for converting to and from network byte-order buffer
    memcpy(&tmp, &time_base, sizeof(double));
    htonll_buffer(tmp, buf);
    fwrite(buf, sizeof(uint8_t), sizeof(buf), f);

    // Temporarily write frame_count as a placeholder to be overwritten later
    int frame_count = 0;
    fwrite(&frame_count, sizeof(int), 1, f);

    // -- Data body --
    dHashContext dhash_ctx = dhash_context(video->context->pix_fmt,
                                           video->context->width,
                                           video->context->height);
    AVPacket pkt;
    av_init_packet(&pkt);
    while (next_video_frame(video, &pkt, dhash_ctx.source) == 0)
    {
        uint64_t hash = dhash(&dhash_ctx);
        htonll_buffer(hash, buf);
        fwrite(buf, sizeof(uint8_t), sizeof(buf), f);

        int64_t timestamp = av_frame_get_best_effort_timestamp(dhash_ctx.source);
        htonll_buffer(timestamp, buf);
        fwrite(buf, sizeof(uint8_t), sizeof(buf), f);

        ++frame_count;
        av_packet_unref(&pkt);
    }
    // Seek to the location of frame_count and overwrite it
    frame_count = htonl(frame_count);
    fseek(f, FRAME_COUNT_LOC, SEEK_SET);
    fwrite(&frame_count, sizeof(int), 1, f);

    fclose(f);
    free_dhash_context(&dhash_ctx);

    return 0;
}

int init_file_context(FileContext *context, const char *filename)
{
    context->file = fopen(filename, "rb");
    context->filename = filename;
    if (context->file == NULL)
    {
        fprintf(stderr, "error: could not open \"%s\": %s\n", filename, strerror(errno));
        return -2;
    }

    if (compare_magic_number(context->file) != 0)
    {
        fprintf(stderr, "error: not a hash file \"%s\"", filename);
        return -1;
    }
    context->data_pos = 0;

    uint8_t buf[sizeof(uint64_t)];

    // Read time_base
    fread(buf, sizeof(uint8_t), sizeof(buf), context->file);
    double time_base;
    uint64_t tmp = btonll(buf);
    memcpy(&time_base, &tmp, sizeof(double));
    context->time_base = time_base;

    // Read frame_count
    int frame_count;
    fread(&frame_count, sizeof(int), 1, context->file);
    frame_count = htonl(frame_count);
    context->frame_count = frame_count;

    return 0;
}

int compare_magic_number(FILE *f)
{
    uint8_t buf[sizeof(magic_number)];

    fread(buf, sizeof(uint8_t), sizeof(buf), f);
    for (uint32_t i = 0; i < sizeof(magic_number); ++i)
    {
        if (buf[i] != magic_number[i])
        {
            return -1;
        }
    }

    return 0;
}

int is_file_hash(const char *filename)
{
    FILE *f = fopen(filename, "rb");
    if (f == NULL)
    {
        return -2;
    }

    int ret = compare_magic_number(f);
    fclose(f);

    return ret;
}

int read_hash_data(FileContext *fc, Hash *data, bool *eof)
{
    int pos = HEADER_SIZE + (fc->data_pos * DATA_SIZE);
    if (pos != ftell(fc->file))
    {
        fseek(fc->file, pos, SEEK_SET);
    }

    uint8_t buf[sizeof(uint64_t)];

    fread(buf, sizeof(uint8_t), sizeof(buf), fc->file);
    data->hash = btonll(buf);

    fread(buf, sizeof(uint8_t), sizeof(buf), fc->file);
    data->timestamp = btonll(buf);

    ++fc->data_pos;

    // Check of EOF has been reached
    if (feof(fc->file) != 0)
    {
        if (*eof == true)
        {
            return -1;
        }
        else
        {
            *eof = true;
        }
        if (fc->data_pos < fc->frame_count)
        {
            printf("warning: less frames than expected read from \"%s\"\n", fc->filename);
            return -4;
        }
    }
    return 0;
}

void close_file_context(FileContext *context)
{
    fclose(context->file);
    context->time_base = 0;
    context->frame_count = 0;
    context->data_pos = 0;
}

int file_to_hash_array(const char *filename, HashArray *hashes)
{
    FileContext fc;

    if (init_file_context(&fc, filename) != 0)
    {
        return -1;
    }
    *hashes = alloc_hash_array(fc.frame_count);

    Hash hash;
    bool eof;
    while (eof == false)
    {
        read_hash_data(&fc, &hash, &eof);
        hash_array_add(hashes, hash);
    }

    close_file_context(&fc);

    return 0;
}
