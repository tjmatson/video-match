/*
 * Copyright (c) 2016 Timothy Matson
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "parser.h"
#include "utils.h"

#include <stdio.h>
#include <stdlib.h>

static struct option long_opts[] =
{
    {"search", required_argument, NULL, 's'},
    {"full", required_argument, NULL, 'f'},
    {"hash", required_argument, NULL, 'H'},
    {"out", required_argument, NULL, 'o'},
    {"threshold", required_argument, NULL, 't'}
};

const char *short_opts = "s:f:H:o:t:";

Options parse(int argc, char **argv)
{
    Options opts = {NULL, NULL, NULL, NULL, NONE, -1};

    while (true)
    {
        int opt = getopt_long(argc, argv, short_opts, long_opts, NULL);
        if (opt == -1) { break; }

        switch (opt)
        {
            case 's':
                opts.search = optarg;
                break;
            case 'f':
                opts.full = optarg;
                break;
            case 'H':
                opts.hash_video = optarg;
                break;
            case 'o':
                opts.hash_out = optarg;
                break;
            case 't':
                if (isdouble(optarg) == false)
                {
                    fprintf(stderr, "error: invalid threshold input \"%s\"\n", optarg);
                    return opts;
                }
                opts.threshold = strtod(optarg, NULL);
                break;
            case '?':
                return opts;
        }
    }

    if (either(opts.hash_video, opts.hash_out) && either(opts.search, opts.full))
    {
        fprintf(stderr, "error: hash and search functions can't be used together\n");
    }
    else if (only_first(opts.search, opts.full))
    {
        fprintf(stderr, "error: no full video file specified\n");
    }
    else if (only_first(opts.full, opts.search))
    {
        fprintf(stderr, "error: no search video file specified\n");
    }
    else if (only_first(opts.hash_video, opts.hash_out))
    {
        fprintf(stderr, "error: no hash output file specified\n");
    }
    else if (only_first(opts.hash_out, opts.hash_video))
    {
        fprintf(stderr, "error: no hash input video file specified\n");
    }
    else if (both(opts.search, opts.full) && neither(opts.hash_video, opts.hash_out))
    {
        opts.action = SEARCH;
    }
    else if (both(opts.hash_video, opts.hash_out) && neither(opts.search, opts.full))
    {
        opts.action = HASH;
    }
    else if (neither(opts.hash_video, opts.hash_out) && neither(opts.search, opts.full))
    {
        // If there are two non-option arguments left, use them as file names
        if (argc - optind == 2)
        {
            opts.search = argv[optind++];
            opts.full = argv[optind];
            opts.action = SEARCH;
        }
        else
        {
            fprintf(stderr, "usage: %s [search-file] [full-file] [options]\n", argv[0]);
        }
    }

    return opts;
}
