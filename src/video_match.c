/*
 * Copyright (c) 2016 Timothy Matson
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "video.h"
#include "hash.h"
#include "io.h"
#include "parser.h"
#include "containers/hash_array.h"

#include <inttypes.h>

int main(int argc, char **argv)
{
    VideoState search_video;
    VideoState full_video;
    VideoState hash_video;
    HashArray search_hashes;
    Options opts;

    opts = parse(argc, argv);
    if (opts.threshold < 0)
    {
        // Default
        opts.threshold = 0.2;
    }

    av_register_all();

    if (opts.action == HASH)
    {
        if (open_video(&hash_video, opts.hash_video) != 0) { return 1; }
        if (write_to_file(&hash_video, opts.hash_out) != 0) { return 1; }
        close_video(&hash_video);

        return 0;
    }
    else if (opts.action == SEARCH)
    {
        if (is_file_hash(opts.search) == 0)
        {
            file_to_hash_array(opts.search, &search_hashes);
        }
        else
        {
            if (open_video(&search_video, opts.search) != 0) { return 1; }
            search_hashes = dhash_video(&search_video);
            close_video(&search_video);
        }

        if (is_file_hash(opts.full) == 0)
        {
            find_matches_from_file(opts.full, &search_hashes, opts.threshold, print_match);
        }
        else
        {
            if (open_video(&full_video, opts.full) != 0) { return 1; }
            find_matches(&full_video, &search_hashes, opts.threshold, print_match);
            close_video(&full_video);
        }

        free_hash_array(&search_hashes);

        return 0;
    }
    else if (opts.action == NONE)
    {
        return 1;
    }
}
