/*
 * Copyright (c) 2016 Timothy Matson
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef VIDEO_MATCH_HASH_H
#define VIDEO_MATCH_HASH_H

#include "video.h"
#include "containers/index_queue.h"
#include "containers/hash_array.h"

#include <libswscale/swscale.h>
#include <libavutil/frame.h>

/* dHash algorithm context */
typedef struct dHashContext
{
    struct SwsContext* swsctx;
    AVFrame *source; // Source frame to transform
    AVFrame *data; // Transformed frame
    uint8_t *sws_buf; // Transformed frame's buffer
    int linesize;  // Linesize of the data frame
    int srcH; // Height of the source frame
} dHashContext;

/* Initialize and allocate a dHashContext.
 * @param  pix_fmt  pixel format of the source video.
 * @param  width    width of the source video>
 * @param  height   height of the source video.
 * @return initialized dHashContext struct
 */
dHashContext dhash_context(enum AVPixelFormat pix_fmt, int width, int height);

/* Free the dHashContext and reset all of its variables. */
void free_dhash_context(dHashContext *context);

/* Hash a video frame using the dHash algorithm.
 * @param   context  pointer to the dHashContext containing the frame to hash.
 * @return  the hash of the frame.
 */
uint64_t dhash(dHashContext *context);

/* Compute the dHash of each frame and store it in the input HashArray.
 * @param  video   input VideoState to hash
 * @return array of the hashes of the video's frames
 */
HashArray dhash_video(VideoState *video);

/* Return the index of the best matching hash in the HashArray to the input hash.
 *
 * The function is a many-to-one comparison of each hash in the HashArray to
 * the single input hash, where each hash will be scored and the index of the
 * best scoring hash will be returned. Hashes are scored based on how similiar
 * it is to the input hash, as well as how close its index is to the last
 * result returned from this function.
 *
 * @param  hash    input hash to compare against
 * @param  hashes  HashArray to compare input to
 * @param  next    the next index that's expected. If -1, the last result
 * returned will be used.
 * @return index of the best matching hash
 */
int index_of_best_scoring_hash(uint64_t hash, HashArray *hashes, int next);

/* Calculate the difference between two hashes based on how many of their
 * bits don't match.
 */
uint8_t bit_difference(uint64_t a, uint64_t b);

/* Calculate the average absolute deviation of an IndexQueue from the sequence
 * {0, 1, 2, ..., n}. E.g., the sequence {1, 2, 1} has an average  deviation of
 * 1, as each integer is either one greater or one lesser than the its
 * corresponding integer in the sequence {0, 1, 2}.
 * |1 - 0| = 1
 * |2 - 1| = 1
 * |1 - 2| = 1
 * (1 + 1 + 1) / 3 = 1
 *
 * This function is used to determine how orderly a sequence of frames are.
 * If a sequence is out of order, or has large gaps in it, it will have a
 * greater absolute average deviation.
 */
double index_sequence_deviation(IndexQueue *indexes);

/* Print the timestamp and deviation of a match */
void print_match(int64_t timestamp, double time_base, double deviation);

/* Find matches of the input HashArray in the VideoState.
 * @param  video      input video to search through
 * @param  hashes     input hashes to search for
 * @param  threshold  maximum deviation for a sequence to be considered a match
 * @param  resultFunc function that will be called when a match is found. The
 *         timestamp, time base, and deviation of the match will be passed.
 */
void find_matches(VideoState *video, HashArray *hashes, double threshold,
        void(*resultFunc)(int64_t, double, double));

/* Find matches of the input HashArray in the hash file.
 * @param  filename   file name of the hash file to search through
 * @param  hashes     input hashes to search for
 * @param  threshold  maximum deviation for a sequence to be considered a match
 * @param  resultFunc function that will be called when a match is found. The
 *         timestamp, time base, and deviation of the match will be passed.
 */
void find_matches_from_file(const char *filename, HashArray *hashes, double threshold,
        void(*resultFunc)(int64_t, double, double));

#endif // VIDEO_MATCH_HASH_H
