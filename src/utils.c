/*
 * Copyright (c) 2016 Timothy Matson
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "utils.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <ctype.h>
#include <locale.h>
#include <errno.h>

char * seconds_to_timestamp(double seconds)
{
    int milli = (int)(modf(seconds, &seconds) * 100);
    int integral = (int)seconds;

    int hour = integral / (60 * 60);
    integral = integral % (60 * 60);
    int min = integral / 60;
    int sec = integral % 60;

    char *str = (char *)malloc(sizeof(char) * 100);
    snprintf(str, 100, "%d:%d:%d.%d", hour, min, sec, milli);

    return str;
}

bool isdouble(char *str)
{
    char *end;
    errno = 0;
    strtod(str, &end);
    if (errno != 0)
    {
        // error returned by strtod
        return false;
    }
    else if (end == str)
    {
        // no characters consumed
        return false;
    }
    else if (*end != 0)
    {
        // trailing unconsumed data
        return false;
    }
    return true;
}

// Logic functions
bool either(const char *a, const char *b)
{
    return (a != NULL || b != NULL);
}

bool both(const char *a, const char *b)
{
    return (a != NULL && b != NULL);
}

bool neither(const char *a, const char *b)
{
    return (a == NULL && b == NULL);
}

bool only_first(const char *a, const char *b)
{
    return (a != NULL && b == NULL);
}
